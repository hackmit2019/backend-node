import passport from 'passport'
import passportjwt from 'passport-jwt'

import { User } from '../models/User'
import logger from '../utils/logger'

import { TOKEN_SECRET } from '../utils/secrets'

passport.use(
  new passportjwt.Strategy(
    {
      jwtFromRequest: passportjwt.ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: TOKEN_SECRET
    },
    (payload, done) => {
      User.findById(payload.id)
        .then(user => {
          if (user) {
            return done(null, user)
          }
          return done(null, false)
        })
        .catch(err => logger.error(err))
    }
  )
)

/**
 * Login Required middleware.
 */
export const isAuthenticated = (req, res, next) => {
  if (req.isAuthenticated()) {
    return next()
  }
  res.json({
    success: false,
    error: 'Invalid token'
  })
}
