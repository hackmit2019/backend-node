import bcrypt from 'bcrypt'
import mongoose from 'mongoose'

const userSchema = new mongoose.Schema(
  {
    fullname: {
      type: String,
      required: true
    },
    email: {
      type: String,
      required: true,
      index: true,
      unique: true
    },
    password: {
      type: String
    }
  },
  { timestamps: true, collection: 'users' }
)

/**
 * Password hash middleware.
 */
userSchema.pre('save', function save(next) {
  const user = this
  if (!user.isModified('password')) {
    return next()
  }
  bcrypt.hash(user.password, 12).then(hash => {
    user.password = hash
    next()
  })
})

const comparePassword = function(candidatePassword) {
  return bcrypt.compare(candidatePassword, this.password)
}

userSchema.methods.comparePassword = comparePassword

export const User = mongoose.model('User', userSchema)
