import bcrypt from 'bcrypt'
import mongoose from 'mongoose'
import axios from 'axios'
import { REV_AI_ENDPOINT } from '../utils/revai'
import { REV_AI_TOKEN } from '../utils/secrets'

const appointmentEntrySchema = new mongoose.Schema(
  {
    name: String,
    recording: Buffer,
    recordingMimetype: String,
    summary: {
      type: mongoose.Types.ObjectId,
      default: null
      // ref: ""
    },
    revaiJobId: {
      type: String,
      default: null
    },
    revaiData: {
      type: Object,
      default: null
    },
    userId: mongoose.Types.ObjectId
  },
  { timestamps: true }
)

appointmentEntrySchema.methods.checkJobStatusAndLoadData = async function() {
  const jobId = this.revaiJobId
  const res = await axios.get(REV_AI_ENDPOINT + '/jobs/' + jobId, {
    headers: {
      Authorization: 'Bearer ' + REV_AI_TOKEN
    }
  })

  console.log(res.data)

  if (res.data.status == 'transcribed') {
    const res1 = await axios.get(
      REV_AI_ENDPOINT + '/jobs/' + jobId + '/transcript',
      {
        headers: {
          Authorization: 'Bearer ' + REV_AI_TOKEN,
          Accept: 'application/vnd.rev.transcript.v1.0+json'
        }
      }
    )

    this.revaiData = res1.data
    await this.save()
  }
}

export const AppointmentEntry = mongoose.model(
  'AppointmentEntry',
  appointmentEntrySchema
)
