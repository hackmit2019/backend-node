import { REV_AI_TOKEN } from './secrets'
import axios from 'axios'
import FormData from 'form-data'
import fs from 'fs'
import { AppointmentEntry } from '../models/AppointmentEntry'

export const REV_AI_ENDPOINT = 'https://api.rev.ai/speechtotext/v1'

// axios.interceptors.request.use(request => {
//   console.log('Starting Request', request)
//   return request
// })

// axios.interceptors.response.use(response => {
//   console.log('Response:', response)
//   return response
// })

export async function analyzeRecording(appointmentDoc) {
  const form = new FormData()
  form.append('media', appointmentDoc.recording, {
    contentType: appointmentDoc.recordingMimetype,
    filename: 'recording.mp3'
  })

  const formHeaders = form.getHeaders()

  const res = await axios.post(REV_AI_ENDPOINT + '/jobs', form, {
    headers: {
      Authorization: 'Bearer ' + REV_AI_TOKEN,
      ...formHeaders
    }
  })
  appointmentDoc.revaiJobId = res.data.id
  await appointmentDoc.save()
}

export async function pollRevaiJobs() {
  const incompleteJobs = await AppointmentEntry.find({ revaiData: null })

  for (const job of incompleteJobs) {
    job.checkJobStatusAndLoadData()
  }
}
