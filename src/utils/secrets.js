import logger from './logger'
import dotenv from 'dotenv'
import fs from 'fs'

if (fs.existsSync('.env')) {
  logger.debug('Using .env file to supply config environment variables')
  dotenv.config({ path: '.env' })
}

export const ENVIRONMENT = process.env.NODE_ENV
const prod = ENVIRONMENT === 'production' // Anything else is treated as 'dev'

export const TOKEN_SECRET = process.env['TOKEN_SECRET']
export const MONGODB_URI = prod
  ? process.env['MONGODB_URI']
  : process.env['MONGODB_URI_LOCAL']

export const REV_AI_TOKEN = process.env['REV_AI_TOKEN']

if (!TOKEN_SECRET) {
  logger.error('No token secret. Set TOKEN_SECRET environment variable.')
  process.exit(1)
}

if (!REV_AI_TOKEN) {
  logger.error('No rev.ai token. Set REV_AI_TOKEN environment variable.')
  process.exit(1)
}

if (!MONGODB_URI) {
  if (prod) {
    logger.error(
      'No mongo connection string. Set MONGODB_URI environment variable.'
    )
  } else {
    logger.error(
      'No mongo connection string. Set MONGODB_URI_LOCAL environment variable.'
    )
  }
  process.exit(1)
}
