import { Promise } from 'bluebird'

export const asyncMiddleware = fn => (req, res, next) => {
  Promise.resolve(fn(req, res)).catch(next)
}
