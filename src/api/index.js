import { Router } from 'express'
import passport from 'passport'
import { asyncMiddleware } from '../utils/async'

const router = Router()

import authRoute from './auth'
import appointmentRoute from './appointment'

router.use('/auth', authRoute)
router.use(
  '/appointment',
  passport.authenticate('jwt', { session: false }),
  appointmentRoute
)

router.get(
  '/user/me',
  passport.authenticate('jwt', { session: false }),
  asyncMiddleware(async (req, res) => {
    res.json({
      user: {
        fullname: req.user.fullname,
        email: req.user.email,
        status: req.user.status
      }
    })
  })
)

export default router
