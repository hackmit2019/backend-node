import { Router } from 'express'
import { asyncMiddleware } from '../utils/async'
import passport from 'passport'
import { User } from '../models/User'
import jwt from 'jsonwebtoken'

import multer from 'multer'
import { AppointmentEntry } from '../models/AppointmentEntry'
import { analyzeRecording } from '../utils/revai'
const storage = multer.memoryStorage()
const upload = multer({ storage: storage })

const router = Router()

router.post(
  '/',
  upload.single('recording'),
  asyncMiddleware(async (req, res) => {
    console.log(req.file)
    const a = new AppointmentEntry({
      name: req.body.name,
      recording: req.file.buffer,
      recordingMimetype: req.file.mimetype,
      userId: req.user._id
    })
    await a.save()
    res.json({ success: true, id: a._id })
    await analyzeRecording(a)
  })
)

router.get(
  '/',
  asyncMiddleware(async (req, res) => {
    const a = await AppointmentEntry.find(
      { userId: req.user._id },
      '_id name createdAt'
    ).lean()

    res.json(a)
  })
)

router.get(
  '/:id',
  asyncMiddleware(async (req, res) => {
    const a = await AppointmentEntry.findById(
      req.params.id,
      '-recording -recordingMimetype -revaiJobId'
    ).lean()

    res.json(a)
  })
)

export default router
