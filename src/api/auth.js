import { Router } from 'express'
import { asyncMiddleware } from '../utils/async'
import passport from 'passport'
import { User } from '../models/User'
import jwt from 'jsonwebtoken'

const router = Router()

router.post(
  '/register',
  asyncMiddleware(async (req, res) => {
    if (!req.body.email || !req.body.fullname || !req.body.password) {
      res.json({
        success: false,
        error: 'Invalid request'
      })
      return
    }

    let user = await User.findOne({
      email: req.body.email
    })

    if (user) {
      res.json({
        success: false,
        error: 'This email is already in use'
      })
    } else {
      const user = new User({
        fullname: req.body.fullname,
        email: req.body.email,
        authProvider: 'local',
        status: 'user'
      })

      user.password = req.body.password

      await user.save()

      const payload = {
        id: user._id,
        fullname: user.fullname,
        email: user.email
      }

      const token = await jwt.sign(payload, req.app.get('secret'), {
        expiresIn: '5d'
      })

      res.json({
        success: true,
        token: token
      })
    }
  })
)

router.post(
  '/login',
  asyncMiddleware(async (req, res) => {
    if (!req.body.email || !req.body.password) {
      res.json({
        success: false,
        error: 'Invalid request'
      })
      return
    }

    let user = await User.findOne({
      email: req.body.email
    })

    if (user) {
      // if (user.authProvider !== 'local') {
      //   res.json({
      //     success: false,
      //     error: `Use social login: ${user.authProvider}`
      //   })
      //   return
      // }

      const doPasswordsMatch = await user.comparePassword(req.body.password)
      if (doPasswordsMatch) {
        const payload = {
          id: user._id,
          fullname: user.fullname,
          email: user.email
        }

        const token = await jwt.sign(payload, req.app.get('secret'), {
          expiresIn: '5d'
        })

        res.json({
          success: true,
          token: token
        })
      } else {
        res.json({
          success: false,
          error: 'Invalid password'
        })
      }
    } else {
      res.json({
        success: false,
        error: 'This user does not exist'
      })
      return
    }
  })
)

router.post(
  '/check',
  passport.authenticate('jwt', { session: false }),
  asyncMiddleware((req, res) => {
    res.json(true)
  })
)

export default router
