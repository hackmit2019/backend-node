import express from 'express'
import compression from 'compression' // compresses requests
import bodyParser from 'body-parser'
import dotenv from 'dotenv'
import path from 'path'
import mongoose from 'mongoose'
import passport from 'passport'
import bluebird from 'bluebird'
import errorhandler from 'errorhandler'
import morgan from 'morgan'
import { pollRevaiJobs } from './utils/revai'
import { MONGODB_URI, TOKEN_SECRET } from './utils/secrets'

// Passport configuration
import './config/passport'

// Create Express server
const app = express()

// Connect to MongoDB
const mongoUrl = MONGODB_URI
mongoose.Promise = bluebird
global.Promise = bluebird

mongoose.set('useCreateIndex', true)
mongoose.set('useFindAndModify', false)

mongoose
  .connect(mongoUrl, { useNewUrlParser: true })
  .then(() => {
    /** ready to use. The `mongoose.connect()` promise resolves to undefined. */
  })
  .catch(err => {
    console.log(
      'MongoDB connection error. Please make sure MongoDB is running. ' + err
    )
    process.exit(1)
  })

// Express configuration
app.set('secret', TOKEN_SECRET)
app.set('port', process.env.PORT || 8080)
app.disable('x-powered-by')
app.use(compression())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(passport.initialize())

if (process.env.NODE_ENV === 'development') {
  // only use in development
  app.use(errorhandler())
  app.use(morgan('combined'))
}

import apiController from './api'

app.use('/api/v1', apiController)

setInterval(pollRevaiJobs, 5000)

export default app
